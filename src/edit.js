import { useState, useEffect } from '@wordpress/element';

const Edit = (props) => {
	const [isJoined, setIsJoined] = useState(
		props.attributes.isJoined || false
	);
	const [groupName, setGroupName] = useState(
		props.attributes.groupName || ''
	);
	const [newgroupName, setNewGroupName] = useState(
		props.attributes.newgroupName || ''
	);
	const [showJoinMessage, setShowJoinMessage] = useState(false);

	// Check if user has already joined the group
	useEffect(() => {
		if (isJoined) {
			// Disable the join button if user has already joined
			const joinButton = document.getElementById('joinButton');
			if (joinButton) {
				joinButton.disabled = false;
			}
		}
	}, [isJoined]);

	const handleJoin = () => {
		props.setAttributes({ isJoined: true, groupName });
		setIsJoined(true);
		setShowJoinMessage(true); // show message when user clicks the button
	};

	const handleJoinNew = () => {
		props.setAttributes({ isJoined: true, groupName: newgroupName });
		setGroupName(newgroupName);
		setIsJoined(true);
		setNewGroupName('');
		setShowJoinMessage(true); // show message when user clicks the button
	};
	const handleGroupNameChange = (event) => {
		setGroupName(event.target.value);
	};

	const handleNewGroupNameChange = (event) => {
		setNewGroupName(event.target.value);
	};

	return (
		<div className={props.className}>
			{isJoined ? (
				<div>
					{showJoinMessage && (
						<div>
							<p>You have joined the {groupName} group!</p>
						</div>
					)}
					<div>
						<p>New Group Name:</p>
						<input
							type="text"
							value={newgroupName}
							onChange={handleNewGroupNameChange}
						/>
						<button id="joinButton" onClick={handleJoinNew}>
							New Join Group
						</button>
					</div>
				</div>
			) : (
				<div>
					<p>Group Name:</p>
					<input
						type="text"
						value={groupName}
						onChange={handleGroupNameChange}
					/>
					<button id="joinButton" onClick={handleJoin}>
						Join Group
					</button>
				</div>
			)}
		</div>
	);
};

export default Edit;
